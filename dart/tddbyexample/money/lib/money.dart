import 'dart:collection';

import 'package:equatable/equatable.dart';

// todo: amount/currency is now back to public, not possible to do in dart what I want with a class extension
class Money extends Equatable implements Expression {
  late final int amount;
  late final String currency;

  Money(this.amount, this.currency);

  bool equals(Money money) {
    return this == money;
  }

  static Money dollar(int amount) {
    return Money(amount, "USD");
  }

  static Money franc(int amount) {
    return Money(amount, "CHF");
  }

  @override
  Expression times(int multiplier) {
    return Money(amount * multiplier, currency);
  }

  @override
  List<Object?> get props => [amount, currency];

  @override
  Expression plus(Expression addend) {
    return Sum(this, addend);
  }

  @override
  String toString() {
    return "$amount $currency";
  }

  @override
  Money convert(Bank bank, String to) {
    var ratedAmount = amount / bank.rate(currency, to);

    return Money(ratedAmount.toInt(), to);
  }
}

abstract class Expression {
  Money convert(Bank bank, String to);
  Expression plus(Expression addend);
  Expression times(int multiplier);
}

class Sum implements Expression {
  Expression augend;
  Expression addend;

  Sum(this.augend, this.addend);

  @override
  Money convert(Bank bank, String to) {
    return Money(addend.convert(bank, to).amount + augend.convert(bank, to).amount, to);
  }

  @override
  Expression plus(Expression addend) {
    return Sum(this, addend);
  }

  @override
  Expression times(int multiplier) {
    return Sum(augend.times(multiplier), addend.times(multiplier));
  }
}

class Bank {
  HashMap rates = HashMap<Pair, int>();

  Money convert(Expression source, String to) {
    return source.convert(this, to);
  }

  void addRate(String from, String to, int rate) {
    rates[Pair(from, to)] = rate;
  }

  int rate(String from, String to) {
    if (from == to) return 1;

    return rates[Pair(from, to)];
  }
}

class Pair extends Equatable {
  final String _from;
  final String _to;

  Pair(this._from, this._to);

  @override
  List<Object?> get props => [_from, _to];
}

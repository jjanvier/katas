import 'package:money/money.dart';
import 'package:test/test.dart';

void main() {

  group('about money', () {
    test('multiplication', () {
      Money five = Money.dollar(5);
      expect(five.times(2), equals(Money.dollar(10)));
      expect(five.times(3), equals(Money.dollar(15)));
    });

    test('equality', () {
      expect(Money.dollar(5).equals(Money.dollar(5)), true);
      expect(Money.dollar(5).equals(Money.dollar(6)), false);
      expect(Money.franc(5).equals(Money.dollar(5)), false);
    });

    test('currency', () {
      expect(Money.dollar(1).currency, "USD");
      expect(Money.franc(1).currency, "CHF");
    });
  });

  group('about sum', () {
    test('plus returns sum', () {
      var five = Money.dollar(5);
      Expression result = five.plus(five);
      Sum sum = result as Sum;
      expect(sum.augend, five);
      expect(sum.addend, five);
    });
  });

  group('about bank', () {
    test('simple addition', () {
      var five = Money.dollar(5);
      Expression sum = five.plus(five);
      var bank = Bank();

      Money converted = bank.convert(sum, "USD");

      expect(converted, equals(Money.dollar(10)));
    });

    test('convert sum', () {
      Expression sum = Sum(Money.dollar(3), Money.dollar(4));
      Bank bank = Bank();
      Money result = bank.convert(sum, "USD");
      expect(result, Money.dollar(7));
    });

    test('convert money', () {
      var bank = Bank();
      Money result = bank.convert(Money.dollar(1), "USD");
      expect(result, equals(Money.dollar(1)));
    });

    test('convert money different currency', () {
      var bank = Bank();
      bank.addRate("CHF", "USD", 2);

      Money result = bank.convert(Money.franc(2), "USD");

      expect(result, equals(Money.dollar(1)));
    });

    test('mixed addition', () {
      Expression fiveBucks = Money.dollar(5);
      Expression tenFrancs = Money.franc(10);
      var bank = Bank();
      bank.addRate("CHF", "USD", 2);
  
      Money result = bank.convert(fiveBucks.plus(tenFrancs), "USD");
  
      expect(result, equals(Money.dollar(10)));
    });
  
    test('sum plus money', () {
      Expression fiveBucks = Money.dollar(5);
      Expression tenFrancs = Money.franc(10);
      var bank = Bank();
      bank.addRate("CHF", "USD", 2);
      Expression sum = Sum(fiveBucks, tenFrancs).plus(fiveBucks);
  
      Money result = bank.convert(sum, "USD");
  
      expect(result, equals(Money.dollar(15)));
    });
  
    test('sum times', () {
      Expression fiveBucks = Money.dollar(5);
      Expression tenFrancs = Money.franc(10);
      var bank = Bank();
      bank.addRate("CHF", "USD", 2);
      Expression sum = Sum(fiveBucks, tenFrancs).times(2);
  
      Money result = bank.convert(sum, "USD");
  
      expect(result, equals(Money.dollar(20)));
    });
  });
}

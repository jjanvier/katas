import 'dart:mirrors';

class WasRun extends TestCase {
  String log = "";

  WasRun(name): super(name);

  void testMethod() {
    log += "testMethod ";
  }

  void testBrokenMethod() {
    throw Exception();
  }

  void setup() {
    log = "setUp ";
  }

  void teardown() {
    log += "teardown";
  }
}

class TestCase {
  String name;

  TestCase(this.name);

  TestResult run(TestResult result) {
    result.testStarted();
    setup();

    try {
      var mirror = reflect(this);
      mirror.invoke(Symbol(name), []);
    } on Exception catch(e) {
      result.testFailed();
    }

    teardown();

    return result;
  }

  void setup() {}

  void teardown() {}
}

class TestResult {
  int runCount = 0;
  int errorCount = 0;

  testStarted() {
    runCount++;
  }

  String summary() {
    return "$runCount run, $errorCount failed";
  }

  void testFailed() {
    errorCount++;
  }
}

class TestSuite {
  List<TestCase> tests = [];

  void add(TestCase test) {
    tests.add(test);
  }

  void run(TestResult result) {
    for (var test in tests) {
      test.run(result);
    }
  }
}

import 'package:xunit/xunit.dart';

class TestCaseTest extends TestCase {
  late TestResult result;

  TestCaseTest(String name) : super(name);

  void setup() {
     result = TestResult();
  }

  void testTemplateMethod() {
    var ourTest = WasRun('testMethod');
    ourTest.run(result);
    assert("setUp testMethod teardown" == ourTest.log);
  }

  void testResult() {
    var ourTest = WasRun('testMethod');
    ourTest.run(result);
    assert("1 run, 0 failed" == result.summary());
  }

  void testFailedResult() {
    var ourTest = WasRun('testBrokenMethod');
    ourTest.run(result);
    assert("1 run, 1 failed" == result.summary());
  }

  void testFailedResultFormatting() {
    result.testStarted();
    result.testFailed();
    assert("1 run, 1 failed" == result.summary());
  }

  void testSuite() {
    var suite = TestSuite();
    suite.add(WasRun('testMethod'));
    suite.add(WasRun('testBrokenMethod'));

    suite.run(result);

    assert("2 run, 1 failed" == result.summary());
  }
}

void main() {
  var result = TestResult();
  var suite = TestSuite();

  suite.add(TestCaseTest("testTemplateMethod"));
  suite.add(TestCaseTest("testResult"));
  suite.add(TestCaseTest("testFailedResult"));
  suite.add(TestCaseTest("testFailedResultFormatting"));
  suite.add(TestCaseTest("testSuite"));

  suite.run(result);
  print(result.summary());
}

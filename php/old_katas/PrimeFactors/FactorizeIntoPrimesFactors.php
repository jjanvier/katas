<?php

declare(strict_types=1);

namespace Jjanvier\Kata\PrimeFactors;

// A list of prime factors is here https://en.wikipedia.org/wiki/Table_of_prime_factors
class FactorizeIntoPrimesFactors
{
    /** @var FindPrimeNumbers */
    private $findPrimeNumbers;

    /** @var int[] */
    private $primeNumbers = [];

    public function __construct(FindPrimeNumbers $findPrimeNumbers)
    {
        $this->findPrimeNumbers = $findPrimeNumbers;
    }

    public function factorize(int $number, $primeFactors = []): array
    {
        if ($number < 1) {
            throw new \Exception('Only positive numbers are accepted');
        }

        if (empty($this->primeNumbers)) {
            $this->primeNumbers = $this->findPrimeNumbers->upTo($number);
        }

        if (in_array($number, $this->primeNumbers)) {
            $primeFactors[] = $number;

            return $primeFactors;
        }

        foreach ($this->primeNumbers as $primeNumber) {
            if ($this->isDivisableBy($number, $primeNumber)) {
                $primeFactors[] = $primeNumber;

                return $this->factorize($number/$primeNumber, $primeFactors);
            }
        }
    }

    private function isDivisableBy($number, $divideBy): bool
    {
        return 0 === $number % $divideBy;
    }
}

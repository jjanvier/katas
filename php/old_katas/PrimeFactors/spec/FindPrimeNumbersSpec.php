<?php

namespace spec\Jjanvier\Kata\PrimeFactors;

use Jjanvier\Kata\PrimeFactors\FindPrimeNumbers;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FindPrimeNumbersSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(FindPrimeNumbers::class);
    }

    function it_accepts_only_positive_integers()
    {
        $this->shouldThrow(\Exception::class)->during('upTo', [0]);
        $this->shouldThrow(\Exception::class)->during('upTo', [-10]);
    }
    function it_finds_prime_numbers_up_to_2()
    {
        $this->upTo(2)->shouldReturn([2]);
    }

    function it_finds_prime_numbers_up_to_3()
    {
        $this->upTo(3)->shouldReturn([2, 3]);
    }

    function it_finds_prime_numbers_up_to_4()
    {
        $this->upTo(4)->shouldReturn([2, 3]);
    }

    function it_finds_prime_numbers_up_to_5()
    {
        $this->upTo(6)->shouldReturn([2, 3, 5]);
    }

    function it_finds_prime_numbers_up_to_6()
    {
        $this->upTo(6)->shouldReturn([2, 3, 5]);
    }

    function it_finds_prime_numbers_up_to_9()
    {
        $this->upTo(9)->shouldReturn([2, 3, 5, 7]);
    }

    function it_finds_prime_numbers_up_to_12()
    {
        $this->upTo(12)->shouldReturn([2, 3, 5, 7, 11]);
    }

    function it_finds_prime_numbers_up_to_15()
    {
        $this->upTo(15)->shouldReturn([2, 3, 5, 7, 11, 13]);
    }

    function it_finds_the_25_first_prime_numbers()
    {
        $this->upTo(100)->shouldReturn(
            [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
        );
    }
}

<?php

namespace spec\Jjanvier\Kata\PrimeFactors;

use Jjanvier\Kata\PrimeFactors\FactorizeIntoPrimesFactors;
use Jjanvier\Kata\PrimeFactors\FindPrimeNumbers;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class FactorizeIntoPrimesFactorsSpec extends ObjectBehavior
{
    function let(FindPrimeNumbers $findPrimeNumbers)
    {
        $this->beConstructedWith($findPrimeNumbers);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(FactorizeIntoPrimesFactors::class);
    }

    function it_accepts_only_positive_integers()
    {
        $this->shouldThrow(\Exception::class)->during('factorize', [0]);
        $this->shouldThrow(\Exception::class)->during('factorize', [-10]);
    }

    function it_factorizes_2($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(2)->willReturn([2]);
        $this->factorize(2)->shouldReturn([2]);
    }

    function it_factorizes_3($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(3)->willReturn([2, 3]);
        $this->factorize(3)->shouldReturn([3]);
    }

    function it_factorizes_4($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(4)->willReturn([2, 3]);
        $this->factorize(4)->shouldReturn([2, 2]);
    }

    function it_factorizes_6($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(6)->willReturn([2, 3, 5]);
        $this->factorize(6)->shouldReturn([2, 3]);
    }

    function it_factorizes_9($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(9)->willReturn([2, 3, 5, 7]);
        $this->factorize(9)->shouldReturn([3, 3]);
    }

    function it_factorizes_12($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(12)->willReturn([2, 3, 5, 7, 11]);
        $this->factorize(12)->shouldReturn([2, 2, 3]);
    }

    function it_factorizes_15($findPrimeNumbers)
    {
        $findPrimeNumbers->upTo(15)->willReturn([2, 3, 5, 7, 11, 13]);
        $this->factorize(15)->shouldReturn([3, 5]);
    }
}

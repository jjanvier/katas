<?php

namespace Jjanvier\Kata\PrimeFactors;

class FindPrimeNumbers
{
    private const FIRST_PRIME_NUMBER = 2;
    private $number;
    private $primeNumbers = [self::FIRST_PRIME_NUMBER];

    public function upTo(int $number): array
    {
        if ($number < 1) {
            throw new \Exception('Only positive numbers are accepted');
        }

        $this->number = $number;

        return $this->findPrimeNumbersFrom(self::FIRST_PRIME_NUMBER);
    }

    private function findPrimeNumbersFrom(int $number): array
    {
        if (!$this->numberDivisableByAPrimeFactor($number)) {
            $this->primeNumbers[] = $number;
        }

        if ($this->number === $number) {
            return $this->primeNumbers;
        }

        return $this->findPrimeNumbersFrom(++$number);
    }

    private function numberDivisableByAPrimeFactor($number): bool
    {
        foreach ($this->primeNumbers as $primeFactor) {
            if (0 === $number % $primeFactor) {
                return true;
            }
        }

        return false;
    }
}

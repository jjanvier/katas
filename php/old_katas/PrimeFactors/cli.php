<?php

use Jjanvier\Kata\PrimeFactors\FactorizeIntoPrimesFactors;
use Jjanvier\Kata\PrimeFactors\FindPrimeNumbers;

require __DIR__ . '/../../vendor/autoload.php';

$factorize = new FactorizeIntoPrimesFactors(new FindPrimeNumbers());

echo "> Find prime factors of: ";
$input = read_stdin();
$primeFactors = $factorize->factorize($input);
echo "> " . implode(', ', $primeFactors) . PHP_EOL;

function read_stdin()
{
    $fr = fopen("php://stdin", "r");
    $input = fgets($fr, 128);
    $input = rtrim($input);
    fclose($fr);

    return $input;
}


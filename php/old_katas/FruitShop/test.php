<?php

require '../../vendor/autoload.php';

$fruitShopTest = new \Jjanvier\Kata\FruitShop\FruitShopTest();
$tests = get_class_methods($fruitShopTest);

foreach ($tests as $test) {
    echo "Test $test\n";
    $fruitShopTest->$test();
}

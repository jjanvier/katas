<?php


namespace Jjanvier\Kata\FruitShop;

class FruitShop
{
    /** @var int */
    private $total = 0;

    private $cherriesCount = 0;
    private $bananasCount = 0;
    private $pommesCount = 0;
    private $meleCount = 0;
    private $allApplesCount = 0;
    private $fruitsCount = 0;

    private const APPLES = 100;
    private const CHERRIES = 75;
    private const BANANAS = 150;

    private const TWO_PACK_CHERRIES_DISCOUNT = 20;
    private const FOUR_PACK_APPLES_DISCOUNT = 100;
    private const FIVE_PACK_FRUITS_DISCOUNT = 200;


    public function cash(string $products): int
    {
        $allProducts = explode(',', $products);
        $allProducts = array_map(function ($product) { return trim($product); }, $allProducts);

        foreach ($allProducts as $product) {
            $this->cashOneProduct($product);
        }

        return $this->total;
    }

    private function cashOneProduct(string $product): int
    {
        $this->fruitsCount++;

        switch ($product) {
            case 'Apples':
                $this->allApplesCount++;
                $this->total += self::APPLES;
                break;
            case 'Pommes':
                $this->allApplesCount++;
                $this->pommesCount++;
                $this->total += $this->pricePommes();
                break;
            case 'Mele':
                $this->allApplesCount++;
                $this->meleCount++;
                $this->total += $this->priceMele();
                break;
            case 'Bananas':
                $this->bananasCount++;
                $this->total += $this->priceBananas();
                break;
            case 'Cherries':
                $this->cherriesCount++;
                $this->total += $this->priceCherries();
                break;
        }

        // "global" discount
        if (4 === $this->allApplesCount) {
            $this->allApplesCount = 0;
            $this->total -= self::FOUR_PACK_APPLES_DISCOUNT;
        }

        if (5 === $this->fruitsCount) {
            $this->fruitsCount = 0;
            $this->total -= self::FIVE_PACK_FRUITS_DISCOUNT;
        }

        return $this->total;
    }

    /**
     * 3 packs of Pomme cost 2€
     * which means
     * third pack is free
     */
    private function pricePommes(): int
    {
        if (3 === $this->pommesCount) {
            $this->pommesCount = 0;
            $price = 0; // third pommes pack is free
        } else {
            $price = self::APPLES;
        }

        return $price;
    }

    /**
     * 2 packs of Mele cost 1€
     * which means
     * second pack is free
     */
    private function priceMele(): int
    {
        if (2 === $this->meleCount) {
            $this->meleCount = 0;
            $price = 0; // second mele pack is free
        } else {
            $price = self::APPLES;
        }

        return $price;
    }

    private function priceCherries(): int
    {
        if (2 === $this->cherriesCount) {
            $this->cherriesCount = 0;
            $price = self::CHERRIES - self::TWO_PACK_CHERRIES_DISCOUNT;
        } else {
            $price = self::CHERRIES;
        }

        return $price;
    }

    private function priceBananas(): int
    {
        if (2 === $this->bananasCount) {
            $this->bananasCount = 0;
            $price = 0; // second banana pack is free
        } else {
            $price = self::BANANAS;
        }

        return $price;
    }
}

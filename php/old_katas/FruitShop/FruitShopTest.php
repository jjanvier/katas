<?php

namespace Jjanvier\Kata\FruitShop;

use Assert\Assertion;

class FruitShopTest
{
    /*
    //NOT VALID IN ITERATION 2
    public function iteration1_test1()
    {
        $fruitShop = new FruitShop();
        
        Assertion::eq($fruitShop->cash('Apples'), 100);
        Assertion::eq($fruitShop->cash('Cherries'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 250);
    }
    */

    /*
    //NOT VALID IN ITERATION 2
    public function iteration1_test2()
    {
        $fruitShop = new FruitShop();
        
        Assertion::eq($fruitShop->cash('Cherries'), 75);
        Assertion::eq($fruitShop->cash('Apples'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 250);
        Assertion::eq($fruitShop->cash('Bananas'), 400);
        Assertion::eq($fruitShop->cash('Apples'), 500);
    }
    */

    /*
    //NOT VALID IN ITERATION 6
    public function iteration2_offers_test1()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Apples'), 100);
        Assertion::eq($fruitShop->cash('Cherries'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 230);
    }
    */

    /*
    //NOT VALID IN ITERATION 6
    public function iteration2_offers_test2()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Cherries'), 75);
        Assertion::eq($fruitShop->cash('Apples'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 230);
        Assertion::eq($fruitShop->cash('Bananas'), 380);
        Assertion::eq($fruitShop->cash('Cherries'), 455);
        Assertion::eq($fruitShop->cash('Cherries'), 510);
        Assertion::eq($fruitShop->cash('Apples'), 610);
    }
    */

    /*
    //NOT VALID IN ITERATION 4
    public function iteration3_new_offer_test1()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Cherries'), 75);
        Assertion::eq($fruitShop->cash('Cherries'), 120);
        Assertion::eq($fruitShop->cash('Bananas'), 270);
        Assertion::eq($fruitShop->cash('Bananas'), 270);
    }
    */


    /*
    //NOT VALID IN ITERATION 4
    public function iteration3_new_offer_test2()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Cherries'), 75);
        Assertion::eq($fruitShop->cash('Apples'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 220);
        Assertion::eq($fruitShop->cash('Bananas'), 370);
        Assertion::eq($fruitShop->cash('Apples'), 470);
        Assertion::eq($fruitShop->cash('Bananas'), 470);
        Assertion::eq($fruitShop->cash('Cherries'), 545);
    }
    */

    /*
    //NOT VALID IN ITERATION 6
    public function iteration4_localization_test1()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Cherries'), 75);
        Assertion::eq($fruitShop->cash('Pommes'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 230);
        Assertion::eq($fruitShop->cash('Bananas'), 380);
        Assertion::eq($fruitShop->cash('Bananas'), 380);
    }
    */

    /*
    //NOT VALID IN ITERATION 6
    public function iteration4_localization_test2()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Cherries'), 75);
        Assertion::eq($fruitShop->cash('Pommes'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 230);
        Assertion::eq($fruitShop->cash('Bananas'), 380);
        Assertion::eq($fruitShop->cash('Apples'), 480);
        Assertion::eq($fruitShop->cash('Mele'), 580);
    }
    */

    /*
    //NOT VALID IN ITERATION 5'
    public function iteration5_localization_offer_test()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Mele'), 100);
        Assertion::eq($fruitShop->cash('Pommes'), 200);
        Assertion::eq($fruitShop->cash('Pommes'), 300);
        Assertion::eq($fruitShop->cash('Apples'), 400);
        Assertion::eq($fruitShop->cash('Pommes'), 400);
        Assertion::eq($fruitShop->cash('Mele'), 450);
        Assertion::eq($fruitShop->cash('Cherries'), 525);
        Assertion::eq($fruitShop->cash('Cherries'), 580);
    }
    */

    /*
    //NOT VALID IN ITERATION 6
    public function iteration5bis_csv_localization_offer_test1()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Mele, Pommes, Pommes, Apples, Pommes, Mele, Cherries, Cherries , Bananas'), 680);
    }
    */

    /*
    //NOT VALID IN ITERATION 6
    public function iteration5bis_csv_localization_offer_test2()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Cherries, Pommes'), 175);
        Assertion::eq($fruitShop->cash('Cherries'), 230);
        Assertion::eq($fruitShop->cash('Pommes, Apples, Bananas'), 580);
        Assertion::eq($fruitShop->cash('Apples, Pommes'), 680);
        Assertion::eq($fruitShop->cash('Mele'), 780);
        Assertion::eq($fruitShop->cash('Apples'), 880);
    }
    */

    public function iteration6_all_apples_pack_test1()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Mele, Pommes, Pommes, Mele'), 200);
        Assertion::eq($fruitShop->cash('Bananas'), 150);
        Assertion::eq($fruitShop->cash('Mele, Pommes, Pommes, Apples, Mele'), 150);
    }

    public function iteration6_all_apples_pack_test2()
    {
        $fruitShop = new FruitShop();

        Assertion::eq($fruitShop->cash('Mele, Pommes, Apples, Pommes, Mele'), 100);
        Assertion::eq($fruitShop->cash('Bananas'), 250);
    }
}


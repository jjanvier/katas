<?php

while (true) {
    echo "> ";
    $command = read_stdin();
    echo "> ". $command . PHP_EOL;
    sleep(1);
}

function read_stdin() {
    $fr = fopen("php://stdin", "r");
    $input = fgets($fr, 128);
    $input = rtrim($input);
    fclose ($fr);

    return $input;
}


<?php

namespace Jjanvier\Kata\LeapYear;

class LeapYear
{
    public function isLeapYear(int $year)
    {
        if (0 === $year % 100 && 0 !== $year % 400) {
            return false;
        }

        return 0 === $year % 4;
    }
}

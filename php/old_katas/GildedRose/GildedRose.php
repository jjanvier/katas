<?php

namespace Jjanvier\Kata\GildedRose;

use Jjanvier\Kata\GildedRose\Item\Item;

class GildedRose
{
    /** @var Item[] */
    private $items;

    function __construct(array $items)
    {
        $this->items = $items;
    }

    function update_quality()
    {
        foreach ($this->items as $item) {
            $item->updateInventory();
        }
    }
}

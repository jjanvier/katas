<?php

namespace spec\Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime\Conjured;
use Jjanvier\Kata\GildedRose\Item\Item;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ConjuredSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('Conjured Mana Cake', 3, 6);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Conjured::class);
    }

    function it_is_an_item()
    {
        $this->shouldImplement(Item::class);
    }

    function it_provides_its_inventory_data()
    {
        $this->inventoryData()->shouldReturn('Conjured Mana Cake, 3, 6');
    }

    function it_decreases_quality_and_sellin_when_updating_inventory()
    {
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Conjured Mana Cake, 2, 4');
    }

    function it_does_not_decreases_quality_when_it_is_already_at_minimum()
    {
        $this->beConstructedWith('Conjured Mana Cake', 3, 0);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Conjured Mana Cake, 2, 0');
    }

    function it_does_not_decreases_quality_below_the_minimu_possible()
    {
        $this->beConstructedWith('Conjured Mana Cake', 3, 1);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Conjured Mana Cake, 2, 0');
    }

    function it_decreases_quality_twice_when_sell_date_has_passed()
    {
        $this->beConstructedWith('Conjured Mana Cake', 0, 5);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Conjured Mana Cake, -1, 1');
    }

    function it_does_not_decreases_below_minimum_even_when_sell_date_has_passed()
    {
        $this->beConstructedWith('Conjured Mana Cake', 0, 1);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Conjured Mana Cake, -1, 0');
    }
}

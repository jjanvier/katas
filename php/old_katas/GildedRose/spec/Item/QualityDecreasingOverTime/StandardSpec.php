<?php

namespace spec\Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;
use Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime\Standard;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StandardSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('+5 Dexterity Vest', 10, 20);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Standard::class);
    }

    function it_is_an_item()
    {
        $this->shouldImplement(Item::class);
    }

    function it_provides_its_inventory_data()
    {
        $this->inventoryData()->shouldReturn('+5 Dexterity Vest, 10, 20');
    }

    function it_decreases_quality_and_sellin_when_updating_inventory()
    {
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('+5 Dexterity Vest, 9, 19');
    }

    function it_does_not_decreases_quality_when_it_is_already_at_minimum()
    {
        $this->beConstructedWith('+5 Dexterity Vest', 10, 0);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('+5 Dexterity Vest, 9, 0');
    }

    function it_decreases_quality_twice_when_sell_date_has_passed()
    {
        $this->beConstructedWith('+5 Dexterity Vest', 0, 5);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('+5 Dexterity Vest, -1, 3');
    }

    function it_does_not_decreases_below_minimum_even_when_sell_date_has_passed()
    {
        $this->beConstructedWith('+5 Dexterity Vest', 0, 1);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('+5 Dexterity Vest, -1, 0');
    }
}

<?php

namespace spec\Jjanvier\Kata\GildedRose\Item;

use Jjanvier\Kata\GildedRose\Item\Item;
use Jjanvier\Kata\GildedRose\Item\Legendary;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LegendarySpec extends ObjectBehavior
{
    public function let()
    {
        $this->beConstructedWith('Sulfuras, Hand of Ragnaros', 0, 80);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Legendary::class);
    }

    function it_is_an_item()
    {
        $this->shouldImplement(Item::class);
    }

    function it_provides_its_inventory_data()
    {
        $this->inventoryData()->shouldReturn('Sulfuras, Hand of Ragnaros, 0, 80');
    }

    function it_does_not_change_quality_or_sell_in_when_updating_inventory()
    {
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Sulfuras, Hand of Ragnaros, 0, 80');
    }
}

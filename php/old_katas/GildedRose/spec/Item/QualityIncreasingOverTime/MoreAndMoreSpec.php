<?php

namespace spec\Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;
use Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime\MoreAndMore;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class MoreAndMoreSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('Backstage passes', 20, 10);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(MoreAndMore::class);
    }

    function it_is_an_item()
    {
        $this->shouldImplement(Item::class);
    }

    function it_provides_its_inventory_data()
    {
        $this->inventoryData()->shouldReturn('Backstage passes, 20, 10');
    }

    function it_increases_quality_and_decreases_sellin_when_updating_inventory()
    {
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, 19, 11');
    }

    function it_does_not_increase_quality_when_it_has_reached_its_maximum()
    {
        $this->beConstructedWith('Backstage passes', 20, 50);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, 19, 50');
    }

    function it_increases_quality_when_sellin_approaches()
    {
        $this->beConstructedWith('Backstage passes', 10, 5);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, 9, 7');
    }

    function it_increases_quality_when_sellin_approaches_but_still_without_exceeding_the_maximum()
    {
        $this->beConstructedWith('Backstage passes', 10, 49);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, 9, 50');
    }

    function it_increases_even_more_quality_when_sellin_is_really_close()
    {
        $this->beConstructedWith('Backstage passes', 5, 5);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, 4, 8');
    }

    function it_increases_even_more_quality_when_sellin_is_really_close_but_still_without_exceeding_the_maximum()
    {
        $this->beConstructedWith('Backstage passes', 5, 49);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, 4, 50');
    }

    function it_drops_quality_when_sell_in_has_passed()
    {
        $this->beConstructedWith('Backstage passes', 0, 40);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Backstage passes, -1, 0');
    }
}

<?php

namespace spec\Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;
use Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime\Standard;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StandardSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith('Aged Brie', 2, 0);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(Standard::class);
    }

    function it_is_an_item()
    {
        $this->shouldImplement(Item::class);
    }

    function it_provides_its_inventory_data()
    {
        $this->inventoryData()->shouldReturn('Aged Brie, 2, 0');
    }

    function it_increases_quality_and_decreases_sellin_when_updating_inventory()
    {
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Aged Brie, 1, 1');
    }

    function it_does_not_increase_quality_when_it_has_reached_its_maximum()
    {
        $this->beConstructedWith('Aged Brie', 2, 50);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Aged Brie, 1, 50');
    }

    function it_inreases_quality_twice_when_sell_date_has_passed()
    {
        $this->beConstructedWith('Aged Brie', 0, 5);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Aged Brie, -1, 7');
    }

    function it_inreases_quality_twice_only_when_sell_date_has_been_reached()
    {
        $this->beConstructedWith('Aged Brie', 1, 1);
        $this->updateInventory();
        $this->inventoryData()->shouldReturn('Aged Brie, 0, 2');
    }
}

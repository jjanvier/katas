<?php

namespace Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;

class MoreAndMore extends QualityIncreasingOverTime implements Item
{
    private const SELL_IN_DATE_REALLY_CLOSE = 5;
    private const SELL_IN_DATE_APPROACHING = 10;
    private const QUALITY_INCREASE_FOR_SELL_IN_DATE_REALLY_CLOSE = 3;
    private const QUALITY_INCREASE_FOR_SELL_IN_DATE_APPROACHING = 2;

    public function updateInventory(): void
    {
        $this->item->sell_in--;

        if ($this->hasSellDatePassed()) {
            $this->item->quality = 0;
        } elseif ($this->isSellDateReallyClose()) {
            $this->increaseQualityBy(self::QUALITY_INCREASE_FOR_SELL_IN_DATE_REALLY_CLOSE);
        } elseif ($this->isSellDateApproaching()) {
            $this->increaseQualityBy(self::QUALITY_INCREASE_FOR_SELL_IN_DATE_APPROACHING);
        } else {
            $this->increaseQuality();
        }
    }

    private function increaseQualityBy(int $increase)
    {
        do {
            $this->increaseQuality();
        } while(--$increase > 0);
    }

    private function isSellDateReallyClose(): bool
    {
        return $this->item->sell_in < self::SELL_IN_DATE_REALLY_CLOSE;
    }

    private function isSellDateApproaching(): bool
    {
        return $this->item->sell_in < self::SELL_IN_DATE_APPROACHING;
    }
}

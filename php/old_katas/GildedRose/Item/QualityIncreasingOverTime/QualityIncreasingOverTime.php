<?php

namespace Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;
use Jjanvier\Kata\GildedRose\Item as LegacyItem;

abstract class QualityIncreasingOverTime implements Item
{
    private const MAXIMUM_QUALITY = 50;

    /** @var LegacyItem */
    protected $item;

    public function __construct(string $name, int $sellIn, int $quality)
    {
        $this->item = new LegacyItem($name, $sellIn, $quality);
    }

    public function inventoryData(): string
    {
        return (string)$this->item;
    }

    protected function increaseQuality(): void
    {
        if ($this->item->quality < self::MAXIMUM_QUALITY) {
            $this->item->quality++;
        }
    }

    protected function hasSellDatePassed(): bool
    {
        return $this->item->sell_in < 0;
    }
}

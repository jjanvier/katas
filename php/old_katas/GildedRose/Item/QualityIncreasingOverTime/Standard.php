<?php

namespace Jjanvier\Kata\GildedRose\Item\QualityIncreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;

class Standard extends QualityIncreasingOverTime implements Item
{
    public function updateInventory(): void
    {
        $this->item->sell_in--;

        if ($this->hasSellDatePassed()) {
            $this->increaseQuality();
            $this->increaseQuality();
        } else {
            $this->increaseQuality();
        }
    }
}

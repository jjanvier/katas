<?php

namespace Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime;

use Jjanvier\Kata\GildedRose\Item as LegacyItem;
use Jjanvier\Kata\GildedRose\Item\Item;

abstract class QualityDecreasingOverTime implements Item
{
    protected const MINIMUM_QUALITY = 0;

    /** @var LegacyItem */
    protected $item;

    public function __construct(string $name, int $sellIn, int $quality)
    {
        $this->item = new LegacyItem($name, $sellIn, $quality);
    }

    public function inventoryData(): string
    {
        return (string)$this->item;
    }

    public function updateInventory(): void
    {
        $this->item->sell_in--;

        if ($this->hasSellDatePassed()) {
            $this->degradeQuality();
            $this->degradeQuality();

        } else {
            $this->degradeQuality();
        }
    }

    protected abstract function degradeQuality(): void;

    private function hasSellDatePassed(): bool
    {
        return $this->item->sell_in < 0;
    }
}

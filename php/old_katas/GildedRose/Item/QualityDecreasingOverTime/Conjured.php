<?php

namespace Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;

class Conjured extends QualityDecreasingOverTime implements Item
{
    protected function degradeQuality(): void
    {
        if ($this->item->quality > self::MINIMUM_QUALITY) {
            $this->item->quality--;
        }
        if ($this->item->quality > self::MINIMUM_QUALITY) {
            $this->item->quality--;
        }
    }
}

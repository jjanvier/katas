<?php

namespace Jjanvier\Kata\GildedRose\Item\QualityDecreasingOverTime;

use Jjanvier\Kata\GildedRose\Item\Item;

class Standard extends QualityDecreasingOverTime implements Item
{
    protected function degradeQuality(): void
    {
        if ($this->item->quality > self::MINIMUM_QUALITY) {
            $this->item->quality--;
        }
    }
}

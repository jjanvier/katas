<?php

namespace Jjanvier\Kata\GildedRose\Item;

interface Item
{
    public function inventoryData(): string;
    public function updateInventory(): void;
}

<?php

namespace Jjanvier\Kata\GildedRose\Item;

use Jjanvier\Kata\GildedRose\Item as LegacyItem;

/**
 * Legendary items never has to be sold or decreases in quality.
 */
class Legendary implements Item
{
    /** @var LegacyItem */
    private $item;

    public function __construct(string $name, int $sellIn, int $quality)
    {
        $this->item = new LegacyItem($name, $sellIn, $quality);
    }

    public function inventoryData(): string
    {
        return (string)$this->item;
    }

    public function updateInventory(): void
    {
        // nothing to do here!
        // legendary item never has to be sold or decreases in quality
    }
}

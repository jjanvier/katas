<?php

use Jjanvier\Kata\GildedRose\GildedRose;
use Jjanvier\Kata\GildedRose\Item;

require __DIR__ . '/../../vendor/autoload.php';

echo "OMGHAI!\n";

$items = array(
    new Item\QualityDecreasingOverTime\Standard('+5 Dexterity Vest', 10, 20),
    new Item\QualityIncreasingOverTime\Standard('Aged Brie', 2, 0),
    new Item\QualityDecreasingOverTime\Standard('Elixir of the Mongoose', 5, 7),
    new Item\Legendary('Sulfuras, Hand of Ragnaros', 0, 80),
    new Item\Legendary('Sulfuras, Hand of Ragnaros', -1, 80),
    new Item\QualityIncreasingOverTime\MoreAndMore('Backstage passes to a TAFKAL80ETC concert', 15, 20),
    new Item\QualityIncreasingOverTime\MoreAndMore('Backstage passes to a TAFKAL80ETC concert', 10, 49),
    new Item\QualityIncreasingOverTime\MoreAndMore('Backstage passes to a TAFKAL80ETC concert', 5, 49),
    new Item\QualityDecreasingOverTime\Conjured('Conjured Mana Cake', 3, 6)
);

$app = new GildedRose($items);

$days = 2;
if (count($argv) > 1) {
    $days = (int) $argv[1];
}

for ($i = 0; $i < $days; $i++) {
    echo("-------- day $i --------\n");
    echo("name, sellIn, quality\n");
    foreach ($items as $item) {
        echo $item->inventoryData() . PHP_EOL;
    }
    echo PHP_EOL;
    $app->update_quality();
}

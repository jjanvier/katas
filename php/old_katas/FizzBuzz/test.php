<?php

require '../../vendor/autoload.php';

$fizzBuzzTest = new \Jjanvier\Kata\FizzBuzz\FizzBussTest();
$tests = get_class_methods($fizzBuzzTest);

foreach ($tests as $test) {
    echo "Test $test\n";
    $fizzBuzzTest->$test();
}

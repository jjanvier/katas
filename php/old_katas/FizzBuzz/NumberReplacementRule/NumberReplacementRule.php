<?php

namespace Jjanvier\Kata\FizzBuzz\NumberReplacementRule;

interface NumberReplacementRule
{
    public function divisibleBy(): int;
    public function replaceBy(): string;
}

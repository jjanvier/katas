<?php

namespace Jjanvier\Kata\FizzBuzz\NumberReplacementRule;

class FizzBuzz implements NumberReplacementRule
{
    public function divisibleBy(): int
    {
        return 15;
    }

    public function replaceBy(): string
    {
        return 'FizzBuzz';
    }
}

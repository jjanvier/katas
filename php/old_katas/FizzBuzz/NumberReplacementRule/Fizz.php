<?php

namespace Jjanvier\Kata\FizzBuzz\NumberReplacementRule;

class Fizz implements NumberReplacementRule
{
    public function divisibleBy(): int
    {
        return 3;
    }

    public function replaceBy(): string
    {
        return 'Fizz';
    }
}

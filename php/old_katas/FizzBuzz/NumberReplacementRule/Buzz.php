<?php

namespace Jjanvier\Kata\FizzBuzz\NumberReplacementRule;

class Buzz implements NumberReplacementRule
{
    public function divisibleBy(): int
    {
        return 5;
    }

    public function replaceBy(): string
    {
        return 'Buzz';
    }
}

<?php

require __DIR__ . '/../../vendor/autoload.php';

$fizzBuzz = new Jjanvier\Kata\FizzBuzz\FizzBuzz([
    new Jjanvier\Kata\FizzBuzz\NumberReplacementRule\Fizz(),
    new Jjanvier\Kata\FizzBuzz\NumberReplacementRule\Buzz(),
    new Jjanvier\Kata\FizzBuzz\NumberReplacementRule\FizzBuzz(),
]);
foreach ($fizzBuzz->upTo(100) as $number) {
    echo $number . "\n";
}

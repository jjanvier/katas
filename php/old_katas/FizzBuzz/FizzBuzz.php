<?php

namespace Jjanvier\Kata\FizzBuzz;

use Jjanvier\Kata\FizzBuzz\NumberReplacementRule\NumberReplacementRule;

class FizzBuzz
{
    /** @var NumberReplacementRule[] */
    private $rules;

    public function __construct(array $rules)
    {
        $this->rules = $rules;
    }

    public function upTo(int $upTo)
    {
        $results = [];
        $counter = 1;

        do {
            $result = $counter;
            foreach ($this->rules as $rule) {
                if (0 === $counter % $rule->divisibleBy()) {
                    $result = $rule->replaceBy();
                }
            }

            $results[] = $result;
        } while (++$counter <= $upTo);

        return $results;
    }
}

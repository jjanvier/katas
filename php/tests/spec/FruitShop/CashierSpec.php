<?php

namespace spec\Jjanvier\Kata\FruitShop;

use Jjanvier\Kata\FruitShop\Cashier;
use PhpSpec\ObjectBehavior;

class CashierSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(Cashier::class);
    }

    function it_cashes_fruits_and_gives_the_total()
    {
        $this->cash('Mele, Pommes, Pommes, Mele')->shouldReturn(200);
        $this->cash('Bananas')->shouldReturn(150);
        $this->cash('Mele, Pommes, Pommes, Apples, Mele')->shouldReturn(150);
    }

    function it_cashes_fruits_and_gives_the_total_second_example()
    {
        $this->cash('Mele, Pommes, Apples, Pommes, Mele')->shouldReturn(100);
        $this->cash('Bananas')->shouldReturn(250);
    }
}

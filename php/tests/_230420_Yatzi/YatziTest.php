<?php

namespace test\Jjanvier\Kata\_230420_Yatzi;

use Jjanvier\Kata\_230420_Yatzi\Category;
use Jjanvier\Kata\_230420_Yatzi\Dice;
use Jjanvier\Kata\_230420_Yatzi\Yatzi;
use PHPUnit\Framework\TestCase;

/**
 * TODO:
 *
 * DONE:
 *  - chance
 *  - small straight
 *  - Large straight
 *  - Yatzy
 *  - Ones
 *  - Twos
 *  - Threes
 *  - Fours
 *  - Fives
 *  - Sixes
 *  - Three of a kind
 *  - Four of a kind
 *  - Pair
 *  - Full house
 *  - Two pairs
 */
class YatziTest extends TestCase
{
    /**
     * @dataProvider twoPairsProvider
     */
    public function test_it_scores_two_pairs(int $expected, array $dice)
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice($dice[0], $dice[1], $dice[2], $dice[3], $dice[4]), Category::TWO_PAIRS);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider fullHouseProvider
     */
    public function test_it_scores_full_house(int $expected, array $dice)
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice($dice[0], $dice[1], $dice[2], $dice[3], $dice[4]), Category::FULL_HOUSE);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider pairProvider
     */
    public function test_it_scores_pairs(int $expected, array $dice)
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice($dice[0], $dice[1], $dice[2], $dice[3], $dice[4]), Category::PAIR);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider fourOfAKindProvider
     */
    public function test_it_scores_four_of_a_kind(int $expected, array $dice)
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice($dice[0], $dice[1], $dice[2], $dice[3], $dice[4]), Category::FOUR_OF_A_KIND);
        $this->assertEquals($expected, $actual);
    }

    /**
     * @dataProvider threeOfAKindProvider
     */
    public function test_it_scores_three_of_a_kind(int $expected, array $dice)
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice($dice[0], $dice[1], $dice[2], $dice[3], $dice[4]), Category::THREE_OF_A_KIND);
        $this->assertEquals($expected, $actual);
    }

    public function test_it_scores_ones()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(1, 1, 5, 1, 6), Category::ONES);
        $this->assertEquals(3, $actual);
    }

    public function test_it_scores_twos()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(2, 1, 5, 2, 6), Category::TWOS);
        $this->assertEquals(4, $actual);
    }

    public function test_it_scores_threes()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(3, 3, 5, 2, 6), Category::THREES);
        $this->assertEquals(6, $actual);
    }

    public function test_it_scores_fours()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(4, 3, 4, 4, 6), Category::FOURS);
        $this->assertEquals(12, $actual);
    }

    public function test_it_scores_fives()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(5, 3, 4, 5, 6), Category::FIVES);
        $this->assertEquals(10, $actual);
    }

    public function test_it_scores_sixes()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(5, 3, 4, 5, 6), Category::SIXES);
        $this->assertEquals(6, $actual);
    }

    public function test_it_scores_yatzi()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(1, 1, 1, 1, 1,), Category::YATZI);
        $this->assertEquals(50, $actual);
    }

    public function test_it_scores_small_straight()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice(1, 2, 3, 4, 5,), Category::SMALL_STRAIGHT);
        $this->assertEquals(15, $actual);
    }

    public function test_it_scores_large_straight()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice( 2, 3, 4, 5, 6,),  Category::LARGE_STRAIGHT);
        $this->assertEquals(20, $actual);
    }

    public function test_it_scores_no_large_straight()
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice( 2, 3, 4, 5, 5,),  Category::LARGE_STRAIGHT);
        $this->assertEquals(0, $actual);
    }

    /**
     * @dataProvider chanceDataProvider
     */
    public function test_it_scores_a_chance(int $expected, array $dice)
    {
        $game = new Yatzi();
        $actual = $game->roll(new Dice($dice[0], $dice[1], $dice[2], $dice[3], $dice[4]), Category::CHANCE);
        $this->assertEquals($expected, $actual);
    }

    public function chanceDataProvider(): array
    {
        return [
            [14, [1,1,3,3,6]],
            [21, [4,5,5,6,1]],
            [5, [1,1,1,1,1]],
        ];
    }

    public function threeOfAKindProvider()
    {
        return [
            [9, [3,3,3,4,5]],
            [0, [3,3,4,5,6]],
            [9, [3,3,3,3,1]],
        ];
    }

    public function fourOfAKindProvider()
    {
        return [
            [8, [2,2,2,2,5]],
            [0, [2,2,2,5,5]],
            [8, [2,2,2,2,2]],
        ];
    }

    public function pairProvider()
    {
        return [
            [0, [1,2,3,4,5]],
            [8, [3,3,3,4,4]],
            [8, [4,4,3,3,3]],
            [12, [1,1,6,2,6]],
            [6, [3,3,3,4,1]],
            [6, [3,3,3,3,1]],
        ];
    }

    public function fullHouseProvider()
    {
        return [
            [8, [1,1,2,2,2]],
            [0, [2,2,3,3,4]],
            [0, [4,4,4,4,4]],
        ];
    }

    public function twoPairsProvider()
    {
        return [
            [8, [1,1,2,3,3]],
            [0, [1,1,2,3,4]],
            [6, [1,1,2,2,2]],
            [0, [3,3,3,3,1]],
        ];
    }
}

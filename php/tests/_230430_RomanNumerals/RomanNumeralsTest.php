<?php

namespace test\Jjanvier\Kata\_230430_RomanNumerals;

use Jjanvier\Kata\_230430_RomanNumerals\RomanNumerals;
use PHPUnit\Framework\TestCase;

class RomanNumeralsTest extends TestCase
{
    /**
     * @test
     */
    public function it_converts_regular_symbols()
    {
        $roman = new RomanNumerals();

        $this->assertEquals('I', $roman->convert(1));
        $this->assertEquals('V', $roman->convert(5));
        $this->assertEquals('X', $roman->convert(10));
        $this->assertEquals('L', $roman->convert(50));
        $this->assertEquals('C', $roman->convert(100));
        $this->assertEquals('D', $roman->convert(500));
        $this->assertEquals('M', $roman->convert(1000));
    }

    /**
     * @test
     * @dataProvider numberProvider
     */
    public function it_converts_numbers(string $expected, int $number)
    {
        $roman = new RomanNumerals();

        $this->assertEquals($expected, $roman->convert($number));
    }

    public function numberProvider()
    {
        return [
            // adds I after base numbers
            'II' => ['II', 2],
            'III' => ['III', 3],
            'VI' => ['VI', 6],
            'VII' => ['VII', 7],
            'VIII' => ['VIII', 8],
            'XI' => ['XI', 11],
            'XII' => ['XII', 12],
            'LI' => ['LI', 51],
            'CI' => ['CI', 101],
            'DI' => ['DI', 501],
            'MI' => ['MI', 1001],
            // more complex number
            'CX' => ['CX', 110],
            'CXIII' => ['CXIII', 113],
            // no more than 3 I
            'IV' => ['IV', 4],
            'IX' => ['IX', 9],
            'CCCLXXIX' => ['CCCLXXIX', 379],
        ];
    }
}

<?php

namespace test\Jjanvier\Kata\_220927_ClosestToZero;

use Jjanvier\Kata\_220927_ClosestToZero\ClosestToZero;
use PHPUnit\Framework\TestCase;

class ClosestToZeroTest extends TestCase
{
    /**
     * @test
     */
    public function the_closest_integer_is_the_min_with_a_list_of_positive_integers()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([1, 2, 3, 4]);

        $this->assertEquals(1, $actual);
    }

    /**
     * @test
     */
    public function the_closest_integer_is_the_non_zero_min_with_a_list_of_positive_integers_containing_zero()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([2, 3, 4, 0]);

        $this->assertEquals(2, $actual);
    }

    /**
     * @test
     */
    public function the_closest_integer_is_the_max_with_a_list_of_negative_integers()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([-1, -2, -3, -4]);

        $this->assertEquals(-1, $actual);
    }

    /**
     * @test
     */
    public function the_closest_integer_is_the_non_zero_max_with_a_list_of_negative_integers_containing_zero()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([-1, -2, 0, -3, -4]);

        $this->assertEquals(-1, $actual);
    }

    /**
     * @test
     */
    public function the_closest_integer_with_negative_or_positive_integers()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([-89, -45, -1, 2, 45]);

        $this->assertEquals(-1, $actual);
    }

    /**
     * @test
     */
    public function the_closest_integer_with_tie_is_always_the_positive_integer()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([-89, -45, -1, 1, 2, 45]);

        $this->assertEquals(1, $actual);
    }

    /**
     * @test
     */
    public function the_closest_integer_with_tie_is_always_the_positive_integer_no_matter_the_order_of_the_list()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestIntegers([45, 2, 1, -1, -45, -89]);

        $this->assertEquals(1, $actual);
    }

    /**
     * @test
     */
    public function the_closest_string_is_the_one_with_less_different_characters()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestStrings([
            'four', // 4 different characters
            'loop', // 3 different characters
            'aaaa', // 1 different character
        ]);

        $this->assertEquals('aaaa', $actual);
    }

    /**
     * @test
     */
    public function the_closest_string_is_the_shortest_one_in_case_of_tie()
    {
        $sut = new ClosestToZero();

        $actual = $sut->closestStrings([
            'eleven', // 4 different characters
            'four', // 4 different characters
            'ptttttttttyz', // 4 different characters
        ]);

        $this->assertEquals('four', $actual);
    }
}

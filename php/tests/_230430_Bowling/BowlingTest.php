<?php

namespace test\Jjanvier\Kata\_230430_Bowling;

use Jjanvier\Kata\_230430_Bowling\Bowling;
use PHPUnit\Framework\TestCase;

/**
 * TODO:
 *  - handle non consecutive strikes
 *  - handle first frame is strike
 *  - handle last frame is strike
 *  - handle consecutive strikes
 *  - handle 10 frames
 */
class BowlingTest extends TestCase
{
    /**
     * @test
     */
    public function it_scores_when_there_are_no_spares_or_strikes()
    {
        $bowling = new Bowling();
        $bowling->roll(5);
        $bowling->roll(2);
        $bowling->roll(3);

        $this->assertEquals(10, $bowling->score());
    }

    /**
     * @test
     */
    public function it_scores_when_there_are_spares()
    {
        $bowling = new Bowling();
        $bowling->roll(3);
        $bowling->roll(7);
        $bowling->roll(4);
        $bowling->roll(6);
        $bowling->roll(5);
        $bowling->roll(3);

        $this->assertEquals(37, $bowling->score());
    }

    /**
     * @test
     */
    public function it_scores_when_there_are_strikes()
    {
        $bowling = new Bowling();
        $bowling->roll(10);
        $bowling->roll(2);
        $bowling->roll(1);

        $this->assertEquals(16, $bowling->score());
    }
}

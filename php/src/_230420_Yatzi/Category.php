<?php

namespace Jjanvier\Kata\_230420_Yatzi;

enum Category
{
    case CHANCE;
    case SMALL_STRAIGHT;
    case LARGE_STRAIGHT;
    case YATZI;
    case ONES;
    case TWOS;
    case THREES;
    case FOURS;
    case FIVES;
    case SIXES;
    case THREE_OF_A_KIND;
    case FOUR_OF_A_KIND;
    case PAIR;
    case FULL_HOUSE;
    case TWO_PAIRS;
}

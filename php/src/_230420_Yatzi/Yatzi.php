<?php

namespace Jjanvier\Kata\_230420_Yatzi;

class Yatzi
{

    public function __construct()
    {
    }

    public function roll(Dice $dice, Category $category): int
    {
        if ($category === Category::YATZI && $dice->areAllDiceEquals()) {
            return 50;
        }

        if ($category === Category::PAIR && $dice->hasPair()) {
            return $dice->sumOfHighestPair();
        }

        if ($category === Category::TWO_PAIRS && $dice->hasTwoPairs()) {
            return $dice->sumOfAllPairs();
        }

        if ($category === Category::ONES) {
            return $dice->sumOf(1);
        }

        if ($category === Category::TWOS) {
            return $dice->sumOf(2);
        }

        if ($category === Category::THREES) {
            return $dice->sumOf(3);
        }

        if ($category === Category::FOURS) {
            return $dice->sumOf(4);
        }

        if ($category === Category::FIVES) {
            return $dice->sumOf(5);
        }

        if ($category === Category::SIXES) {
            return $dice->sumOf(6);
        }

        if ($category === Category::CHANCE) {
            return $dice->sum();
        }

        if ($category === Category::SMALL_STRAIGHT && $dice->isSmallStraight()) {
            return $dice->sum();
        }

        if ($category === Category::LARGE_STRAIGHT && $dice->isLargeStraight()) {
            return $dice->sum();
        }

        if ($category === Category::THREE_OF_A_KIND && $dice->hasThreeOfKind()) {
            return $dice->sumOfThreeOfAKind();
        }

        if ($category === Category::FOUR_OF_A_KIND && $dice->hasFourOfAKind()) {
            return $dice->sumOfFourOfAKind();
        }

        if ($category === Category::FULL_HOUSE && $dice->hasFullHouse()) {
            return $dice->sum();
        }

        return 0;
    }
}

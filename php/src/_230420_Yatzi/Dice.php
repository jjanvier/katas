<?php

namespace Jjanvier\Kata\_230420_Yatzi;

class Dice
{
    public function __construct(private int $a, private int $b, private int $c, private int $d, private int $e)
    {
    }

    public function areAllDiceEquals(): bool
    {
        return $this->a === $this->b && $this->b === $this->c && $this->c === $this->d && $this->d === $this->e;
    }

    public function isSmallStraight(): bool
    {
        return $this->a === 1 && $this->b === 2 && $this->c === 3 && $this->d === 4 && $this->e === 5;
    }

    public function isLargeStraight(): bool
    {
        return $this->a === 2 && $this->b === 3 && $this->c === 4 && $this->d === 5 && $this->e === 6;
    }

    public function sum(): int
    {
        return $this->a + $this->b + $this->c + $this->d + $this->e;
    }

    public function sumOf(int $value): int
    {
        $numbers = array_filter(
            [$this->a, $this->b, $this->c, $this->d, $this->e],
            fn (int $number) => $number === $value
        );

        return array_sum($numbers);
    }

    public function hasThreeOfKind(): bool
    {
        return $this->hasValueOfAKind(3);
    }

    public function sumOfThreeOfAKind(): int
    {
        return $this->sumOfXOfAKind(3);
    }

    public function hasFourOfAKind(): bool
    {
        return $this->hasValueOfAKind(4);
    }

    public function sumOfFourOfAKind(): int
    {
        return $this->sumOfXOfAKind(4);
    }

    private function sumOfXOfAKind(int $numberOfValuesExpected): int|float
    {
        $counters = $this->countValues();

        $value = 0;
        foreach ($counters as $key => $counter) {
            if ($counter >= $numberOfValuesExpected) {
                $value = $key;
            }
        }

        return $value * $numberOfValuesExpected;
    }

    private function hasValueOfAKind(int $expectedValue): bool
    {
        $counters = $this->countValues();

        foreach ($counters as $counter) {
            if ($counter >= $expectedValue) {
                return true;
            }
        }

        return false;
    }

    private function countValues(): array
    {
        return array_count_values([$this->a, $this->b, $this->c, $this->d, $this->e]);
    }

    public function hasPair(): bool
    {
        return $this->hasValueOfAKind(2);
    }

    public function sumOfHighestPair(): int
    {
        $counters = $this->countValues();

        $value = 0;
        foreach ($counters as $key => $counter) {
            if ($counter >= 2 && $key > $value) {
                $value = $key;
            }
        }

        return $value * 2;
    }

    public function hasFullHouse(): bool
    {
        return $this->hasThreeOfKind() && $this->hasPair() && !$this->areAllDiceEquals();
    }

    public function hasTwoPairs(): bool
    {
        $pairCount = 0;
        $counters = $this->countValues();

        foreach ($counters as $counter) {
            if ($counter >= 2) {
                $pairCount++;
            }
        }

        return $pairCount === 2;
    }

    public function sumOfAllPairs(): int
    {
        $counters = $this->countValues();

        $sum = 0;
        foreach ($counters as $key => $counter) {
            if ($counter >= 2) {
                $sum += ($key*2);
            }
        }

        return $sum;
    }
}

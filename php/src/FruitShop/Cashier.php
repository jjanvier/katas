<?php

namespace Jjanvier\Kata\FruitShop;

class Cashier
{
    const APPLE_PRICE = 100;

    const CHERRIES_PRICE = 75;
    const CHERRIES_DISCOUNT_FOR_TWO_PACKS = 20;
    const PACK_COUNT_FOR_CHERRIES_DISCOUNT = 2;

    const BANANAS = 150;
    const PACK_COUNT_FOR_BANANAS_DISCOUNT = 2;
    const FOUR_APPLE_PACKS_DISCOUNT = 100;
    const FIVE_FRUITS_DISCOUNT = 200;

    private int $total;
    private int $cherriesCount;
    private int $bananasCount;
    private int $pommesCount;
    private int $meleCount;
    private int $appleCount;
    private int $allApplesCountForDiscount;
    private int $allFruitsForDiscount;

    public function __construct()
    {
        $this->total = 0;
        $this->cherriesCount = 0;
        $this->bananasCount = 0;
        $this->pommesCount = 0;
        $this->meleCount = 0;
        $this->appleCount = 0;
        $this->allApplesCountForDiscount = 0;
        $this->allFruitsForDiscount = 0;
    }

    public function cash(string $fruits): int
    {
        $fruitsArray = explode(',',$fruits);

        foreach ($fruitsArray as $fruit) {
            switch (trim($fruit)) {
                case 'Apples':
                    $this->appleCount++;
                    $this->allApplesCountForDiscount++;
                    $this->allFruitsForDiscount++;
                    $this->total += self::APPLE_PRICE;
                    break;
                case 'Mele':
                    $this->meleCount++;
                    $this->allApplesCountForDiscount++;
                    $this->allFruitsForDiscount++;
                    $this->total += $this->getMelePrice();
                    break;
                case 'Pommes':
                    $this->pommesCount++;
                    $this->allApplesCountForDiscount++;
                    $this->allFruitsForDiscount++;
                    $this->total += $this->getPommesPrice();
                    break;
                case 'Cherries':
                    $this->cherriesCount++;
                    $this->allFruitsForDiscount++;
                    $this->total += $this->getCherriesPrice();
                    break;
                case 'Bananas':
                    $this->bananasCount++;
                    $this->allFruitsForDiscount++;
                    $this->total += $this->getBananasPrice();
                    break;
            }

            $this->total -= $this->fourApplePacksDiscount();
            $this->total -= $this->fiveFruitsDiscount();
        }

        return $this->total;
    }

    private function getCherriesPrice(): int
    {
        if ($this->cherriesCount % self::PACK_COUNT_FOR_CHERRIES_DISCOUNT === 0) {
            return self::CHERRIES_PRICE - self::CHERRIES_DISCOUNT_FOR_TWO_PACKS;
        }

        return self::CHERRIES_PRICE;
    }

    private function getBananasPrice(): int
    {
        if ($this->bananasCount % self::PACK_COUNT_FOR_BANANAS_DISCOUNT === 0) {
            return 0;
        }

        return self::BANANAS;
    }

    private function getPommesPrice(): int
    {
        if ($this->pommesCount % 3 === 0) {
            return 0;
        }

        return self::APPLE_PRICE;
    }

    private function getMelePrice(): int
    {
        if ($this->meleCount % 2 === 0) {
            return 0;
        }

        return self::APPLE_PRICE;
    }

    private function fourApplePacksDiscount(): int
    {
        if ($this->allApplesCountForDiscount % 4 === 0 && $this->allApplesCountForDiscount > 1) {
            $this->allApplesCountForDiscount = 0;

            return self::FOUR_APPLE_PACKS_DISCOUNT;
        }

        return 0;
    }

    private function fiveFruitsDiscount(): int
    {
        if ($this->allFruitsForDiscount % 5 === 0 && $this->allFruitsForDiscount > 1) {
            $this->allFruitsForDiscount = 0;

            return self::FIVE_FRUITS_DISCOUNT;
        }

        return 0;
    }
}

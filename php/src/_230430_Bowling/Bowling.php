<?php

namespace Jjanvier\Kata\_230430_Bowling;

class Bowling
{
    private int $score = 0;
    /** @var int[] */
    private array $rolls = [];
    public function __construct()
    {
    }

    public function roll(int $pinsDown): void
    {
        $this->rolls[] = $pinsDown;
        $this->score += $pinsDown;

        if ($pinsDown === 10) {
            $this->rolls[] = 0;
        }
    }

    public function score(): int
    {
        for ($rollNumber = 0; $rollNumber < count($this->rolls); $rollNumber += 2) {
            if ($this->isFrameStrike($rollNumber)) {
                $this->score += $this->getStrikeBonus($rollNumber);
            }
            elseif ($this->isFrameFinished($rollNumber) && $this->isNextFrameFinished($rollNumber) && $this->isFrameSpare($rollNumber)) {
                $this->score += $this->getSpareBonus($rollNumber);
            }

        }

        return $this->score;
    }

    public function isFrameSpare(int $rollNumber): bool
    {
        return $this->rolls[$rollNumber] + $this->rolls[$rollNumber + 1] === 10;
    }

    public function getSpareBonus(int $rollNumber): int
    {
        // first roll of next frame
        return $this->rolls[$rollNumber + 2];
    }

    private function isFrameFinished(int $rollNumber): bool
    {
        return array_key_exists($rollNumber, $this->rolls) &&
            array_key_exists($rollNumber + 1, $this->rolls);
    }

    public function isNextFrameFinished(int $rollNumber): bool
    {
        return $this->isFrameFinished($rollNumber + 2);
    }

    public function isFrameStrike(int $rollNumber): bool
    {
        return $this->rolls[$rollNumber] === 10;
    }

    public function getStrikeBonus(int $rollNumber): int
    {
        // sum of two rolls of next frame
        return $this->rolls[$rollNumber + 2] + $this->rolls[$rollNumber + 3];
    }
}

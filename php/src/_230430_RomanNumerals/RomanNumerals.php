<?php

namespace Jjanvier\Kata\_230430_RomanNumerals;

class RomanNumerals
{
    public function convert(int $number): string
    {
        $roman = '';

        $multiples = $this->getMultiples($number);

        foreach ($multiples as $baseNumber => $count) {
            if ($baseNumber === 1 || $baseNumber === 5) {
                continue;
            }
            $roman .= $this->repeatCharacter($baseNumber, $count);
        }

        if ($multiples[5] === 1 && $multiples[1] === 4) {
            $roman .= 'IX';
        }
        elseif ($multiples[5] === 0 && $multiples[1] === 4) {
            $roman .= 'IV';
        } else {
            $roman .= $this->repeatCharacter(5, $multiples[5]);
            $roman .= $this->repeatCharacter(1, $multiples[1]);
        }

        return $roman;
    }

    public function getBaseCharacter(int $number): string
    {
        switch ($number) {
            case 1:
                return 'I';
            case 5:
                return 'V';
            case 10:
                return 'X';
            case 50:
                return 'L';
            case 100:
                return 'C';
            case 500:
                return 'D';
            case 1000:
                return 'M';
        }

        throw new \InvalidArgumentException('Invalid base number');
    }

    private function isMultiple(int $dividend, int $divisor): bool
    {
        return $this->getQuotient($dividend, $divisor) > 0;
    }

    private function repeatCharacter(int $number, int $count): string
    {
        return str_repeat($this->getBaseCharacter($number), $count);
    }

    private function getQuotient(int $dividend, int $divisor): int
    {
        return floor($dividend / $divisor);
    }

    private function stillToFindMultiples(int $number, int $baseNumber): int
    {
        return $number - ($this->getQuotient($number, $baseNumber) * $baseNumber);
    }

    /**
     * @return array
     *
     * Give the count of each roman number for a given arabic number. For instance, for "568", it will return:
     *      [
     *         1000 => 0,
     *         500 => 1,
     *         100 => 0,
     *         50 => 1,
     *         10 => 1,
     *         5 => 1,
     *         1 => 3,
     *      ]
     */
    private function getMultiples(int $number): array
    {
        $mutiples = [];

        foreach ([1000, 500, 100, 50, 10, 5, 1] as $baseNumber) {
            if ($this->isMultiple($number, $baseNumber)) {
                $mutiples[$baseNumber] = $this->getQuotient($number, $baseNumber);
                $number = $this->stillToFindMultiples($number, $baseNumber);
            } else {
                $mutiples[$baseNumber] = 0;
            }
        }

        return $mutiples;
    }
}

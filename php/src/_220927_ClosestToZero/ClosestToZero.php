<?php

namespace Jjanvier\Kata\_220927_ClosestToZero;

use function count_chars;
use function strlen;

class ClosestToZero
{
    public function closestIntegers(array $integers): int
    {
        $integersWithoutZero = array_filter($integers, function ($int) {return $int !== 0;});

        $closest = null;
        foreach ($integersWithoutZero as $int) {
            if ($closest === null) {
                $closest = $int;
                continue;
            }

            if (abs($int) < abs($closest)) {
                $closest = $int;
                continue;
            }

            if (abs($int) === abs($closest) && $int > 0) {
                $closest = $int;
            }
        }

        return $closest;
    }

    public function closestStrings(array $strings): string
    {
        $charactersNumberByWord = $this->countNumberOfDifferentCharactersPerString($strings);

        $eligibleStrings = $this->getStringsWithLeastDifferentCharacters($charactersNumberByWord);

        $closest = null;
        foreach ($eligibleStrings as $word) {
            if ($closest === null) {
                $closest = $word;
                continue;
            }

            if (strlen($word) < strlen($closest)) {
                $closest = $word;
            }
        }

        return $closest;
    }

    /**
     * Return an array containing the number of different characters per string
     * For instance ['loop' => 3, 'eleven' => 4, 'ptttttttttyz' => 4]
     *
     * @param array $strings
     * @return array
     */
    private function countNumberOfDifferentCharactersPerString(array $strings): array
    {
        $charactersNumberByWord = [];
        foreach ($strings as $string) {
            $charactersNumberByWord[$string] = strlen(count_chars($string, 3));
        }

        return $charactersNumberByWord;
    }

    private function getStringsWithLeastDifferentCharacters(array $charactersNumberByWord): array
    {
        $minCharacters = min($charactersNumberByWord);

        $minCharactersNumberByWord = array_filter($charactersNumberByWord, function ($number) use ($minCharacters) {
            return $number === $minCharacters;
        });

        return array_keys($minCharactersNumberByWord);
    }
}
